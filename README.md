<b>Helper program to re-organize files from multiple files into a single folder</b>

HOW TO USE:
- copy binary file (`unfolder`) to your local machine
- if the file does not have executable permission, run `chmod +x unfolder`
- [IMPORTANT] Make sure your source folder exists, default folder name: *source*
- [IMPORTANT] clear all files inside source folder to avoid unwanted overwrite
- copy all folders to be unfolded inside source folder
- [WARNING] This program assumes all files have unique name, otherwise file with same name will be overwritten during the process

EXECUTION:
- `./unfolder -source {{path_to_source_dir}} -destination {{path_to_destination_dir}}`
- example:
    - `./unfolder -source my_source -destination my_destination`: the app will try to unfold all files from my_source folder to my_destination folder
    - `./unfolder `: the app will try to unfold all files from default *source* folder to default *destination* folder

FLAGS:
- `-source` : path_to_input_source_dir, default value; *source/*
- `-destination` : path_to_destination_dir, default value: *destination/*
