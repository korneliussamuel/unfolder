package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

var source, destination string

func init() {
	flag.StringVar(&source, "source", "source/", "source directory containing all folders to be unfolded")
	flag.StringVar(&destination, "destination", "destination/", "destination directory where the files will be unfolded")
	flag.Parse()
	if flag.NArg() != 0 {
		log.Fatalln("Invalid arguments")
	}
}

func main() {
	if err := filepath.Walk(source, unfold); err != nil {
		fmt.Printf("Error: %v\n", err)
		return
	}
	fmt.Println("Files unfolded at:", destination)
}

func unfold(root string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	if info.IsDir() {
		return nil
	}

	sourceFile, err := os.Open(root)
	if err != nil {
		return err
	}
	defer sourceFile.Close()

	destPath := filepath.Join(destination, filepath.Base(root))
	if err := os.MkdirAll(filepath.Dir(destPath), 0755); err != nil {
		return err
	}

	destFile, err := os.Create(destPath)
	if err != nil {
		return err
	}
	defer destFile.Close()

	_, err = io.Copy(destFile, sourceFile)
	return err
}
